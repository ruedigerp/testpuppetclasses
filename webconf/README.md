## nginx vhost config 

### init.pp class files

#### manifests/init.pp for single config file

Use this file if you like have all vhosts in a file.

#### manifests/init2.pp for multiple config files

Use this file if you like have one file for each vhosts.

### erb template files

#### templates/nginx.conf.erb single file template

Use this template if you want to use a single config file.

#### templates/nginx.conf2.erb multi file template

Use this template if you want to using multiple config file.

