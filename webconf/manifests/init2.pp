class webconf {

  define webconfig::config ($cfg) {
    file {
      $cfg[$title]['configname']: 
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0770',
      content => template("webconf/nginx.conf.erb"),
    }
  }

  $webconfigs =  {
    'matrix1.de' => {
      active			=> '1',
      configname	=> '/srv/www/vhosts/nginx1.conf',
      subdomains	=> 'www.matrix1.de',
      webpath 		=> '/srv/www/vhosts',
      ipv6ip  		=> '2010::1',
      php 				=> '1',
      phpport			=>	'10010',
    },
    'matrix2.de' => {
      active      => '0',
      configname  => '/srv/www/vhosts/nginx2.conf',
      subdomains  => 'www.matrix1.de',
      webpath     => '/srv/www/vhosts',
      ipv6ip      => '2010::2',
      php         => '0',
      phpport     => '10010',
    },
  }

  webconfig::config { [ 'matrix1.de', 'matrix2.de' ]:
    cfg => $webconfigs,
  }
}
