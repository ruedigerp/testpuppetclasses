class webconf {

  $cfg = {
    'domain1.de' => {
      'webpath' => '/srv/www/vhosts',
      'subdomains' => 'www.domain1.de test.domain1.de',
      'ipv6ip' => '1002::1',
      'php' => '0',
      'phpport' => '10001',
     },
    'domain2.de' => {
      'webpath' => '/srv/www/vhosts',
      'subdomains' => 'www.domain1.de',
      'ipv6ip' => '1002::2',
      'php' => '0',
      'phpport' => '10001',
     },
  }
  file { [ "/srv/www/vhosts/nginx.test.conf" ]:
    mode => 644,
    owner => www-data,
    group => www-data,
    content => template("webconf/nginx.conf.erb"),
  }

}
